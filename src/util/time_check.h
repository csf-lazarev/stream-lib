 //
// Created by yaroslav on 2/24/24.
//

#ifndef STREAM_LIB_TIME_CHECK_H
#define STREAM_LIB_TIME_CHECK_H

#include <chrono>

uint64_t currentTime();

uint64_t currentTimeMcs();

#endif //STREAM_LIB_TIME_CHECK_H
