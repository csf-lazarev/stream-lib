//
// Created by yaroslav on 4/21/24.
//

#include <algorithm>
#include "stlb_strings.h"


std::string toLower(std::string data) {
    std::transform(data.begin(), data.end(), data.begin(),
                   [](unsigned char c){ return std::tolower(c); } // correct
    );
    return data;
}
