//
// Created by yaroslav on 4/21/24.
//

#ifndef STREAM_LIB_STLB_STRINGS_H
#define STREAM_LIB_STLB_STRINGS_H

#include <string>

std::string toLower(std::string data);

#endif //STREAM_LIB_STLB_STRINGS_H
