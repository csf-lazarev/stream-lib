//
// Created by yaroslav on 4/16/24.
//
#include "uuid.h"

std::string stlb::uuid::generate_uuid_v4() {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution dist(0, 15);
    std::string res;
    for (int i : dash) {
        if (i) res += "-";
        res += v[dist(rng)];
        res += v[dist(rng)];
    }
    return res;
}
