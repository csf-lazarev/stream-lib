//
// Created by yaroslav on 4/16/24.
//

#ifndef STREAM_LIB_UUID_H
#define STREAM_LIB_UUID_H


#include <random>
#include <sstream>

namespace stlb {
    namespace uuid {
        std::string generate_uuid_v4();
        constexpr static const char *v = "0123456789abcdef";
        constexpr static const int dash[] = {0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0 };
    }

    std::string generate_uuid_v4();
}

#endif //STREAM_LIB_UUID_H
