//
// Created by yaroslav on 2/20/24.
//

#ifndef STREAM_LIB_FILTER_PARAMS_H
#define STREAM_LIB_FILTER_PARAMS_H

using filter_param = unsigned int;

namespace stlb {
    const filter_param FP_MAX_FRAME_COUNT = 0x0;
    const filter_param FP_PATHS_TO_READ = 0x1;
    const filter_param FP_SAVE_PREFIX = 0x2;
    const filter_param FP_SCALE_FACTOR = 0x3;
    const filter_param FP_ROTATE_FLAG = 0x4;
}

#endif //STREAM_LIB_FILTER_PARAMS_H
