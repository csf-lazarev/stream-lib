//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_ABSTRACTFILTER_H
#define STREAM_LIB_ABSTRACTFILTER_H


#include "filter.h"

namespace stlb {
    class AbstractFilter : public IFilter {
    private:
        std::string filterUUID;
        std::string filterTypeID_;
    protected:
        void setFilterTypeId(const std::string_view &filterTypeId);
    public:

        AbstractFilter();

        [[nodiscard]] IPipe & getInPipe() override;

        [[nodiscard]] IPipe & getOutPipe() override;

        ~AbstractFilter() override = default;

        IPipe & getPipe(pipe_flag_t flag) override;

        void consume(frame_p frame_ptr) override;

        void produce(frame_p frame_ptr) override;

        void beforeStart() override;

        void afterStop() override;

        [[nodiscard]] std::string filterTypeId() const final;

        [[nodiscard]] std::string filterId() const override;

        void trigger() override;
    };
}


#endif //STREAM_LIB_ABSTRACTFILTER_H
