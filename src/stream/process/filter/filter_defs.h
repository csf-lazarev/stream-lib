//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_FILTER_DEFS_H
#define STREAM_LIB_FILTER_DEFS_H

using pipe_flag_t = unsigned int;

namespace stlb {
    const pipe_flag_t PIPE_IN = 0x0;
    const pipe_flag_t PIPE_OUT = 0x1;
}

#endif //STREAM_LIB_FILTER_DEFS_H
