//
// Created by yaroslav on 2/19/24.
//

#include "AbstractFilter.h"
#include "../../utils/uuid.h"
#include "filter_defs.h"

stlb::IPipe & stlb::AbstractFilter::getInPipe() {
    return getPipe(stlb::PIPE_IN);
}

stlb::IPipe & stlb::AbstractFilter::getOutPipe() {
    return getPipe(stlb::PIPE_OUT);
}

void stlb::AbstractFilter::consume(frame_p frame_ptr) {
// no action
}

void stlb::AbstractFilter::produce(frame_p frame_ptr) {
// no action
}

stlb::IPipe & stlb::AbstractFilter::getPipe(pipe_flag_t flag) {
    return IPipe::empty();
}

void stlb::AbstractFilter::beforeStart() {
// no action
}

void stlb::AbstractFilter::afterStop() {
// no action
}

std::string stlb::AbstractFilter::filterTypeId() const {
    return filterTypeID_;
}

std::string stlb::AbstractFilter::filterId() const {
    return filterUUID;
}

stlb::AbstractFilter::AbstractFilter() {
    filterUUID = uuid::generate_uuid_v4();
}

void stlb::AbstractFilter::trigger() {
    // no action
}

void stlb::AbstractFilter::setFilterTypeId(const std::string_view &filterTypeId) {
    filterTypeID_ = filterTypeId;
}

