//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_FILTER_H
#define STREAM_LIB_FILTER_H

#include <string>
#include "../../data/pipe/IPipe.h"
#include "triggerable.h"
#include "filter_defs.h"
#include "filter_params.h"
#include "../../parameters/IParametrized.h"

namespace stlb {
    class IFilter : public ITriggerable, public IParametrized {
    public:
        virtual void connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) = 0;

        [[nodiscard]] virtual IPipe & getPipe(pipe_flag_t flag) = 0;

        [[nodiscard]] virtual IPipe & getInPipe() = 0;

        [[nodiscard]] virtual IPipe & getOutPipe() = 0;

        ~IFilter() override = default;

        virtual void consume(frame_p frame_ptr) = 0;

        virtual void produce(frame_p frame_ptr) = 0;

        virtual void beforeStart() = 0;

        virtual void afterStop() = 0;

        virtual std::string filterTypeId() const = 0;

        virtual std::string filterId() const = 0;
    };
}


#endif //STREAM_LIB_FILTER_H
