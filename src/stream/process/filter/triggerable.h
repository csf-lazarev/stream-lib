//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_TRIGGERABLE_H
#define STREAM_LIB_TRIGGERABLE_H
namespace stlb {
    class ITriggerable {
    public:
        virtual void trigger() = 0;

        virtual ~ITriggerable() = default;
    };
}

#endif //STREAM_LIB_TRIGGERABLE_H
