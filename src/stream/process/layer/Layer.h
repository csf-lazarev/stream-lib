//
// Created by yaroslav on 4/21/24.
//

#ifndef STREAM_LIB_LAYER_H
#define STREAM_LIB_LAYER_H

#include "ILayer.h"

namespace stlb {

    class Layer : public ILayer {
    public:
        ~Layer() override;
    private:
        std::string _filterType;
        int _parallelism;
        std::vector<std::shared_ptr<RunnableFilter>> _filters;
    protected:
        void addFilter(const std::shared_ptr<RunnableFilter>& filter);
        bool isBuilt();
    public:
        Layer(const std::string_view &filterType, int parallelism);

        [[nodiscard]] std::string filterType() const override;

        [[nodiscard]] int parallelism() const override;

        std::vector<std::shared_ptr<RunnableFilter>> filters() override;

        void run() override;

        void stop() override;
    };

} // stlb

#endif //STREAM_LIB_LAYER_H
