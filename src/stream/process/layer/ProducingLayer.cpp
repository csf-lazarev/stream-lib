//
// Created by yaroslav on 4/21/24.
//

#include "ProducingLayer.h"
#include "Registrar.h"
#include "stream/process/runnable/ProducingRunnableFilter.h"

namespace stlb {
    void ProducingLayer::buildLayer() {
        using namespace stlb::collection;
        if (isBuilt()) {
            return;
        }
        auto &reg = system::Registrar::instance();
        auto supplier = reg.forTypeId(filterType());
        for (int i = 0; i < parallelism(); ++i) {
            addFilter(std::make_shared<ProducingRunnableFilter>(supplier()));
        }
    }
} // stlb