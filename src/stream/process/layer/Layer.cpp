//
// Created by yaroslav on 4/21/24.
//

#include "Layer.h"
#include "Registrar.h"

std::string stlb::Layer::filterType() const {
    return _filterType;
}

int stlb::Layer::parallelism() const {
    return _parallelism;
}


void stlb::Layer::run() {
    std::ranges::for_each(_filters.begin(), _filters.end(), [](auto f){f->run();});
}

void stlb::Layer::stop() {
    std::ranges::for_each(_filters.begin(), _filters.end(), [](auto f){f->stop();});
}

stlb::Layer::Layer(const std::string_view &filterType, int parallelism) : _filterType(filterType),
                                                                          _parallelism(parallelism),
                                                                          _filters(){
}

std::vector<std::shared_ptr<stlb::RunnableFilter>> stlb::Layer::filters() {
    return _filters;
}

void stlb::Layer::addFilter(const std::shared_ptr<RunnableFilter>& filter) {
    _filters.push_back(filter);
}

stlb::Layer::~Layer() = default;

bool stlb::Layer::isBuilt() {
    return !_filters.empty();
}
