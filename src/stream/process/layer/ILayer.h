//
// Created by yaroslav on 4/20/24.
//

#ifndef STREAM_LIB_ILAYER_H
#define STREAM_LIB_ILAYER_H

#include <string>
#include <vector>
#include "../runnable/RunnableFilter.h"
#include "stream/process/item/PipelineItem.h"

namespace stlb {

    class ILayer : public PipelineItem {
    public:
        virtual ~ILayer() = default;

        [[nodiscard]] virtual std::string filterType() const = 0;
        [[nodiscard]] virtual int parallelism() const = 0;
        [[nodiscard]] virtual int maxParallelism() const = 0;
        virtual std::vector<std::shared_ptr<RunnableFilter>> filters() = 0;
        virtual void buildLayer() = 0;
    };

} // stlb

#endif //STREAM_LIB_ILAYER_H
