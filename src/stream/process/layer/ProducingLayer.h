//
// Created by yaroslav on 4/21/24.
//

#ifndef STREAM_LIB_PRODUCINGLAYER_H
#define STREAM_LIB_PRODUCINGLAYER_H

#include "Layer.h"

namespace stlb {

    class ProducingLayer : public Layer {
        void buildLayer() override;
    };

} // stlb

#endif //STREAM_LIB_PRODUCINGLAYER_H
