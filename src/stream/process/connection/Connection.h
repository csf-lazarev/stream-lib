//
// Created by yaroslav on 5/14/24.
//

#ifndef STREAM_LIB_CONNECTION_H
#define STREAM_LIB_CONNECTION_H


#include "stream/process/item/PipelineItem.h"
#include "stream/process/layer/ILayer.h"

namespace stlb {

    class Connection : public PipelineItem {
    public:
        [[nodiscard]] virtual ILayer& in() = 0;
        [[nodiscard]] virtual ILayer out() = 0;
    };

} // stlb

#endif //STREAM_LIB_CONNECTION_H
