//
// Created by yaroslav on 4/10/24.
//

#include "ConsumingRunnableFilter.h"
#include "RunnableFilter.h"
#include "time_check.h"

void stlb::collection::ConsumingRunnableFilter::loop(std::stop_token token) {
    while (isRun()) {
        takeWork();
        if (token.stop_requested()) {
            return;
        }
        auto start = currentTimeMcs();
        auto frame = getFilter().getInPipe().readCurrentFrame();
        consume(frame);
        auto t = static_cast<double >(currentTimeMcs() - start);
        completeNewFrame(1e6 / t);
    }
}

stlb::collection::ConsumingRunnableFilter::ConsumingRunnableFilter() : RunnableFilter(std::shared_ptr<IFilter>(nullptr)) {

}

stlb::collection::ConsumingRunnableFilter::ConsumingRunnableFilter(std::shared_ptr<IFilter> filter) : RunnableFilter(filter) {

}

void stlb::collection::ConsumingRunnableFilter::stop() {
    addWork();
    RunnableFilter::stop();
}
