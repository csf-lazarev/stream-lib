//
// Created by yaroslav on 4/10/24.
//

#ifndef STREAM_LIB_CONSUMINGRUNNABLEFILTER_H
#define STREAM_LIB_CONSUMINGRUNNABLEFILTER_H


#include <thread>
#include "RunnableFilter.h"

namespace stlb::collection {
    class ConsumingRunnableFilter : public stlb::RunnableFilter {

    public:
        ConsumingRunnableFilter();
        ConsumingRunnableFilter(std::shared_ptr<IFilter> filter);

        ~ConsumingRunnableFilter() override = default;

        void stop() override;

    protected:
        void loop(std::stop_token token) override;
    };
}


#endif //STREAM_LIB_CONSUMINGRUNNABLEFILTER_H
