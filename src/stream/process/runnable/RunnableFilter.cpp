//
// Created by yaroslav on 4/10/24.
//

#include "ConsumingRunnableFilter.h"
#include "RunnableFilter.h"

#include <utility>

void stlb::RunnableFilter::attachFilter(const std::shared_ptr<IFilter> &filter) {
    this->filter = filter;
    if (!this->filter->getInPipe().isEmptyPipeInstance()) {
        this->filter->getInPipe().attachTriggerable(this, T_TO);
    }
}

stlb::RunConfig stlb::RunnableFilter::getRunConfig() const {
    return config;
}

void stlb::RunnableFilter::setRunConfig(const RunConfig &config) {
    this->config = config;
}

void stlb::RunnableFilter::run() {
    frameCounter = 1;
    filter->beforeStart();
    runned = true;
    auto supplier = [this] { return std::jthread([this](std::stop_token inner_tok) {
        this->loop(inner_tok);
    }); };
    assignNewThread(supplier);
}

void stlb::RunnableFilter::stop() {
    runned = false;
    runningThread.request_stop();
    runningThread.join();
    filter->afterStop();
}

void stlb::RunnableFilter::trigger() {
    addWork();
    filter->trigger();
}

void stlb::RunnableFilter::setDoubleP(const std::string &name, double p) {
    filter->setDoubleP(name, p);
}

void stlb::RunnableFilter::setBoolP(const std::string &name, bool p) {
    filter->setBoolP(name, p);
}

void stlb::RunnableFilter::setIntP(const std::string &name, int p) {
    filter->setIntP(name, p);
}

void stlb::RunnableFilter::setStringP(const std::string &name, const char *p) {
    filter->setStringP(name, p);
}

void stlb::RunnableFilter::setObjectP(const std::string &name, void *p) {
    filter->setObjectP(name, p);
}

void stlb::RunnableFilter::connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) {
    filter->connectPipe(pipe, flag);
}

stlb::IPipe &stlb::RunnableFilter::getPipe(pipe_flag_t flag) {
    return filter->getPipe(flag);
}

stlb::IPipe &stlb::RunnableFilter::getInPipe() {
    return filter->getInPipe();
}

stlb::IPipe &stlb::RunnableFilter::getOutPipe() {
    return filter->getOutPipe();
}

void stlb::RunnableFilter::consume(frame_p frame_ptr) {
    filter->consume(frame_ptr);
}

void stlb::RunnableFilter::produce(frame_p frame_ptr) {
    filter->produce(frame_ptr);
}

void stlb::RunnableFilter::addWork() {
    workLoad.release();
}

void stlb::RunnableFilter::takeWork() {
    workLoad.acquire();
}

void stlb::RunnableFilter::assignNewThread(const std::function<std::jthread(void)> &threadSupplier) {
    runningThread = std::move(threadSupplier());
}
stlb::RunnableFilter::RunnableFilter(std::shared_ptr<IFilter> filter) : filter(std::move(filter)) {
    analyzeVector = analyze_vector();
}

stlb::IFilter &stlb::RunnableFilter::getFilter() {
    return *filter;
}

bool stlb::RunnableFilter::isRun() const {
    return runned;
}

void stlb::RunnableFilter::setStringP(const std::string &name, const std::string &p) {
    filter->setStringP(name, p);
}

analyze_vector stlb::RunnableFilter::windowValuesBandwidth() const {
    return analyzeVector;
}

void stlb::RunnableFilter::completeNewFrame(double data) {
    analyzeVector.push(data);
    instantBandwidth_ = data;
    averageBandwidth_ = (frameCounter - 1) / (double)frameCounter * averageBandwidth_ + data / static_cast<double>(frameCounter);
    frameCounter++;
}

double stlb::RunnableFilter::averageBandwidth() const {
    return averageBandwidth_;
}

double stlb::RunnableFilter::instantBandwidth() const {
    return instantBandwidth_;
}
