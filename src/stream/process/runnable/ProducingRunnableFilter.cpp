//
// Created by yaroslav on 4/14/24.
//

#include "ProducingRunnableFilter.h"
#include "../../collections/filter/abs/DefaultProduceFilter.h"
#include "time_check.h"

stlb::collection::ProducingRunnableFilter::ProducingRunnableFilter() : stlb::RunnableFilter(std::shared_ptr<IFilter>(nullptr)) {

}

stlb::collection::ProducingRunnableFilter::ProducingRunnableFilter(std::shared_ptr<IFilter> filter) : stlb::RunnableFilter(filter) {

}

stlb::collection::ProducingRunnableFilter::~ProducingRunnableFilter() = default;

void stlb::collection::ProducingRunnableFilter::loop(std::stop_token token) {
    while (isRun()) {
        if (token.stop_requested()) {
            return;
        }
        auto &produceFilter = dynamic_cast<DefaultProduceFilter &>(getFilter());
        if (produceFilter.canProduce()) {
            auto start = currentTimeMcs();
            auto frame = produceFilter.createFrame();
            produce(frame);
            auto t = static_cast<double>(currentTimeMcs() - start);
            completeNewFrame(1e6 / t);
        }
    }
}
