//
// Created by yaroslav on 4/10/24.
//

#ifndef STREAM_LIB_RUNNABLEFILTER_H
#define STREAM_LIB_RUNNABLEFILTER_H


#include <thread>
#include "RunConfig.h"
#include "../filter/AbstractFilter.h"
#include "../../analyze/IAnalyzable.h"
#include "IRunnable.h"

namespace stlb {
    class RunnableFilter : public AbstractFilter, public IAnalyzable, public IRunnable {
    private:
        long frameCounter = 1;
        analyze_vector analyzeVector;
        double averageBandwidth_ = 0;
        double instantBandwidth_ = 0;

        RunConfig config;
        std::shared_ptr<IFilter> filter;

        bool runned = false;
        std::jthread runningThread;
        std::counting_semaphore<10> workLoad = std::counting_semaphore<10>(0);
    public:
        virtual void attachFilter(const std::shared_ptr<IFilter> &filter);

        [[nodiscard]] virtual RunConfig getRunConfig() const;

        virtual void setRunConfig(const RunConfig &config);

        void run() override;

        void stop() override;

        void trigger() override;

        void setDoubleP(const std::string &name, double p) override;

        void setBoolP(const std::string &name, bool p) override;

        void setIntP(const std::string &name, int p) override;

        void setStringP(const std::string &name, const char *p) override;

        void setStringP(const std::string &name, const std::string &p) override;

        void setObjectP(const std::string &name, void *p) override;

        void connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) override;

        IPipe &getPipe(pipe_flag_t flag) override;

        IPipe &getInPipe() override;

        IPipe &getOutPipe() override;

        void consume(frame_p frame_ptr) override;

        void produce(frame_p frame_ptr) override;

        explicit RunnableFilter(std::shared_ptr<IFilter> filter);

        [[nodiscard]] analyze_vector windowValuesBandwidth() const override;

        [[nodiscard]] double averageBandwidth() const override;

        [[nodiscard]] double instantBandwidth() const override;

    protected:

        void completeNewFrame(double data);

        virtual void loop(std::stop_token token) = 0;

        virtual void addWork();

        virtual void takeWork();

        virtual void assignNewThread(const std::function<std::jthread(void)> &threadSupplier);

        IFilter &getFilter();

        [[nodiscard]] bool isRun() const;
    };
}


#endif //STREAM_LIB_RUNNABLEFILTER_H
