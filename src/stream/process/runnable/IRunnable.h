//
// Created by yaroslav on 4/21/24.
//

#ifndef STREAM_LIB_IRUNNABLE_H
#define STREAM_LIB_IRUNNABLE_H

namespace stlb {
    class IRunnable {
        virtual void run() = 0;
        virtual void stop() = 0;
    };
}

#endif //STREAM_LIB_IRUNNABLE_H
