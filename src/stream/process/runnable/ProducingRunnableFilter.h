//
// Created by yaroslav on 4/14/24.
//

#ifndef STREAM_LIB_PRODUCINGRUNNABLEFILTER_H
#define STREAM_LIB_PRODUCINGRUNNABLEFILTER_H


#include "RunnableFilter.h"

namespace stlb::collection {
    class ProducingRunnableFilter : public stlb::RunnableFilter {

    public:
        ProducingRunnableFilter();

        explicit ProducingRunnableFilter(std::shared_ptr<IFilter> filter);

        ~ProducingRunnableFilter() override;

    protected:
        void loop(std::stop_token token) override;
    };
}


#endif //STREAM_LIB_PRODUCINGRUNNABLEFILTER_H
