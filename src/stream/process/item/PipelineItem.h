//
// Created by yaroslav on 5/14/24.
//

#ifndef STREAM_LIB_PIPELINEITEM_H
#define STREAM_LIB_PIPELINEITEM_H

#include "stream/process/runnable/IRunnable.h"
#include "IParametrized.h"

namespace stlb {

    class PipelineItem : public IRunnable, public IParametrized {
    public:
        [[nodiscard]] virtual int order() = 0;
    };

} // stlb

#endif //STREAM_LIB_PIPELINEITEM_H
