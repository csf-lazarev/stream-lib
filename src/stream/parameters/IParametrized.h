#ifndef IPARAMETRIZED_H
#define IPARAMETRIZED_H

#pragma once

#include <string>
#include <functional>

namespace stlb {
    class IParametrized {
    public:
        virtual ~IParametrized();

        virtual void setDoubleP(const std::string &name, double p);

        virtual void setBoolP(const std::string &name, bool p);

        virtual void setIntP(const std::string &name, int p);

        virtual void setStringP(const std::string &name, const char *p);

        virtual void setStringP(const std::string &name, const std::string &p);

        virtual void setObjectP(const std::string &name, void *p);

    };
}

#endif