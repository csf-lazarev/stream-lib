#include "IParametrized.h"

void stlb::IParametrized::setDoubleP(const std::string &name, double p) {
    // do nothing
}

void stlb::IParametrized::setIntP(const std::string &name, int p) {
    // do nothing
}

void stlb::IParametrized::setStringP(const std::string &name, const char *p) {
    // do nothing
}

void stlb::IParametrized::setObjectP(const std::string &name, void *p) {
    // do nothing
}

void stlb::IParametrized::setBoolP(const std::string &name, bool p) {
    // do nothing
}

void stlb::IParametrized::setStringP(const std::string &name, const std::string &p) {
    setStringP(name, p.c_str());
}

stlb::IParametrized::~IParametrized() {

}
