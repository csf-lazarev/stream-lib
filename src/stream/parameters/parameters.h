//
// Created by yaroslav on 4/1/24.
//

#ifndef STREAM_LIB_PARAMETERS_H
#define STREAM_LIB_PARAMETERS_H
#pragma once

namespace stlb {
#define BIND_NOCAST(parameter, value, variable, name) if (parameter == name) { variable = value; return;}
#define BIND(parameter, value, name, variable, type) if (parameter == "name") { variable = *reinterpret_cast<type *>(value); return;}
#define BIND_NOCAST_D(variable, name_) if (name == name_) { variable = p; return;}
#define BIND_D(variable, name_, type) if (name == name_) { variable = *reinterpret_cast<type *>(p); return;}
}

#endif //STREAM_LIB_PARAMETERS_H
