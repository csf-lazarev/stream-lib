//
// Created by yaroslav on 2/26/24.
//
#include "operators.h"

std::shared_ptr<stlb::IPipe> operator>>(std::shared_ptr<stlb::IFilter> left, std::shared_ptr<stlb::IPipe> right) {
    left->connectPipe(right, stlb::PIPE_OUT);
    return right;
}

std::shared_ptr<stlb::IFilter> operator>>(std::shared_ptr<stlb::IPipe> left, std::shared_ptr<stlb::IFilter> right) {
    right->connectPipe(left, stlb::PIPE_IN);
    left->attachTriggerable(&*right, stlb::T_TO);
    return right;
}