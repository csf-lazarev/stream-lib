//
// Created by yaroslav on 4/21/24.
//

#ifndef STREAM_LIB_REGISTRAR_H
#define STREAM_LIB_REGISTRAR_H

#include <string>
#include <map>
#include <memory>
#include <functional>
#include "util/stlb_strings.h"

namespace stlb {
    class IFilter;
}

namespace stlb::system {

    using filter_c = std::function<std::shared_ptr<IFilter>()>;

    class Registrar {
    private:
        std::map<std::string, filter_c, std::less<>> supplierMap;
    public:
        static Registrar &instance() {
            static Registrar INSTANCE;
            return INSTANCE;
        }

        std::string bindFilter(const std::string &typeId, const filter_c& supplier);
        bool isRegistered(const std::string_view &typeId);
        filter_c forTypeId(const std::string_view &typeId);
    };

    template<typename F>
    std::string bindFilter(std::string clazzName) {
        auto lower = toLower(clazzName);
        return Registrar::instance().bindFilter(lower,
                                                []{ return std::make_shared<F>(); });
    }

}

#define STLB_REG_FILTER(clazz) stlb::system::bindFilter<clazz>(#clazz);

#endif //STREAM_LIB_REGISTRAR_H
