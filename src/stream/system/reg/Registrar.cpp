//
// Created by yaroslav on 4/21/24.
//

#include "Registrar.h"

std::string stlb::system::Registrar::bindFilter(const std::string &typeId, const filter_c &supplier) {
    supplierMap[typeId] = supplier;
    return typeId;
}

bool stlb::system::Registrar::isRegistered(const std::string_view &typeId) {
    return supplierMap.contains(typeId);
}

stlb::system::filter_c stlb::system::Registrar::forTypeId(const std::string_view &typeId) {
    if (!isRegistered(typeId)) {
        throw std::exception();
    }
    return supplierMap[std::string(typeId)];
}

// stlb