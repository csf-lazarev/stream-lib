//
// Created by yaroslav on 2/26/24.
//

#ifndef STREAM_LIB_OPERATORS_H
#define STREAM_LIB_OPERATORS_H

#include "data/pipe/IPipe.h"
#include "process/filter/filter.h"

std::shared_ptr<stlb::IPipe> operator>>(std::shared_ptr<stlb::IFilter> left, std::shared_ptr<stlb::IPipe> right);
std::shared_ptr<stlb::IFilter> operator>>(std::shared_ptr<stlb::IPipe> left, std::shared_ptr<stlb::IFilter> right);

#endif //STREAM_LIB_OPERATORS_H
