//
// Created by yaroslav on 2/23/24.
//

#ifndef STREAM_LIB_CIRCULARBUFFER_H
#define STREAM_LIB_CIRCULARBUFFER_H

#include <vector>

namespace stlb {
    template<class T>
    class CircularBuffer {
    private:
        std::vector<T*> storage;
        const size_t bufferSize;
        unsigned long readP = 0;
        unsigned long writeP = 0;

    public:

        explicit CircularBuffer(const size_t bufferSize) : bufferSize(bufferSize), storage(bufferSize) {}

        void write(T **data);

        char read(T **container);

        [[nodiscard]] bool isFull() const;
    };
}

template<class T>
bool stlb::CircularBuffer<T>::isFull() const {
    return writeP - readP >= bufferSize;
}

template<class T>
char stlb::CircularBuffer<T>::read(T **container) {
    if (readP < writeP) {
        *container = storage[readP++];
        return 0;
    }
    return -1;
}

template<class T>
void stlb::CircularBuffer<T>::write(T **data) {
    storage[writeP++] = *data;
}


#endif //STREAM_LIB_CIRCULARBUFFER_H
