//
// Created by yaroslav on 4/6/24.
//

#include "IPipe.h"

stlb::EmptyPipe stlb::IPipe::emptyPipe = EmptyPipe();

stlb::IPipe &stlb::IPipe::empty() {
    return emptyPipe;
}

bool stlb::IPipe::isEmptyPipeInstance() {
    return this == &emptyPipe;
}