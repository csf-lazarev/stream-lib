//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_IPIPE_H
#define STREAM_LIB_IPIPE_H
#pragma once


#include <cstddef>
#include "../frame/Frame.h"
#include "pipe_defs.h"
#include "../../process/filter/triggerable.h"

namespace stlb {
    class EmptyPipe;

    class IPipe {
    private:
        static EmptyPipe emptyPipe;
    public:
        virtual frame_p readCurrentFrame() = 0;

        virtual int writeNewFrame(std::shared_ptr<Frame> frame, write_flag_t flag) = 0;

        virtual void attachTriggerable(ITriggerable *triggerable, trig_flag_t flag) = 0;

        virtual ~IPipe() = default;

        bool isEmptyPipeInstance();

        static IPipe &empty();
    };

    class EmptyPipe : public IPipe {
    public:
        frame_p readCurrentFrame() override {
            return {nullptr};
        }

        int writeNewFrame(std::shared_ptr<Frame> frame, write_flag_t flag) override {
            return -1;
        }

        void attachTriggerable(ITriggerable *triggerable, trig_flag_t flag) override {
            // no action
        }

        ~EmptyPipe() override = default;
    };
}

#endif //STREAM_LIB_IPIPE_H
