//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_PIPE_DEFS_H
#define STREAM_LIB_PIPE_DEFS_H

#include "../frame/Frame.h"

using frame_p = std::shared_ptr<stlb::Frame>;
using trig_flag_t = unsigned int;
using write_flag_t = signed char;

namespace stlb {
    const trig_flag_t T_FROM = 0x0;
    const trig_flag_t T_TO = 0x1;
    const write_flag_t NORMAL = 0x0;
    const write_flag_t FORCE = 0x1;
}

// write new frame even pipe is full, in other words do overwriting
#endif //STREAM_LIB_PIPE_DEFS_H
