//
// Created by yaroslav on 2/19/24.
//

#include "Frame.h"

frame_id_t stlb::Frame::getFrameId() const {
    return frameId;
}

void stlb::Frame::setFrameId(frame_id_t frameId) {
    this->frameId = frameId;
}

std::shared_ptr<st_lib_img> stlb::Frame::getImagePtr() const {
    return imagePtr;
}

stlb::Frame::Frame(frame_id_t frameId, const std::shared_ptr<st_lib_img> &imagePtr) : frameId(frameId), imagePtr(imagePtr) {}
