//
// Created by yaroslav on 2/19/24.
//

#ifndef STREAM_LIB_FRAME_H
#define STREAM_LIB_FRAME_H


#include <cstddef>
#include "frame_defs.h"
#include <opencv4/opencv2/core.hpp>

using st_lib_img = cv::Mat;

namespace stlb {
    class Frame {
    private:
        frame_id_t frameId;
        std::shared_ptr<st_lib_img> imagePtr;
    public:
        [[nodiscard]] size_t getFrameId() const;

        void setFrameId(frame_id_t frameId);

        [[nodiscard]] std::shared_ptr<st_lib_img> getImagePtr() const;

        Frame(frame_id_t frameId, const  std::shared_ptr<st_lib_img> &imagePtr);
    };
}


#endif //STREAM_LIB_FRAME_H
