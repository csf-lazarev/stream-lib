//
// Created by yaroslav on 4/17/24.
//

#ifndef STREAM_LIB_FIXED_QUEUE_H
#define STREAM_LIB_FIXED_QUEUE_H

#include <queue>
#include <deque>
#include <iostream>

namespace stlb {
    template <typename T, int MaxLen, typename Container=std::deque<T>>
    class fixed_queue : public std::queue<T, Container> {
    public:
        void push(const T& value)  {
            if (this->size() == MaxLen) {
                this->c.pop_front();
            }
            std::queue<T, Container>::push(value);
        }
    };
}


#endif //STREAM_LIB_FIXED_QUEUE_H
