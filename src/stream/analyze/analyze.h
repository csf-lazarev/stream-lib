//
// Created by yaroslav on 4/17/24.
//

#ifndef STREAM_LIB_ANALYZE_H
#define STREAM_LIB_ANALYZE_H

namespace stlb {
#ifdef STREAM_LIB_ANALYZE_ROW
    constexpr const int DATA_LENGTH = STREAM_LIB_ANALYZE_ROW;
#else
    constexpr const int DATA_LENGTH = 100;
#endif
}

#endif //STREAM_LIB_ANALYZE_H
