//
// Created by yaroslav on 4/17/24.
//

#ifndef STREAM_LIB_IANALYZABLE_H
#define STREAM_LIB_IANALYZABLE_H


#include <vector>
#include "fixed_queue.h"
#include "analyze.h"

using analyze_vector = stlb::fixed_queue<int, stlb::DATA_LENGTH>;
namespace stlb {
    class IAnalyzable {
    public:
        virtual analyze_vector windowValuesBandwidth() const = 0;
        virtual double averageBandwidth() const = 0;
        virtual double instantBandwidth() const = 0;
    };
}


#endif //STREAM_LIB_IANALYZABLE_H
