//
// Created by yaroslav on 4/14/24.
//

#ifndef STREAM_LIB_FILTERDESCRIPTOR_H
#define STREAM_LIB_FILTERDESCRIPTOR_H

#include "../Descriptor.h"
#include "../../process/filter/filter.h"

template<class Filter>
class FilterDescriptor : public Descriptor<Filter> {

private:
    static_assert(std::is_base_of_v<stlb::IFilter, Filter>, "FilterDescriptor is describing only IFilter");

    std::function<void(Filter &)> parametrizator;

public:

    FilterDescriptor() = delete;

    explicit FilterDescriptor(const std::function<void(Filter &)> &parametrizator) : parametrizator(parametrizator) {}

    std::shared_ptr<Filter> create() override {
        auto filter = std::make_shared<Filter>();
        parametrize(filter);
        return filter;
    }

    void parametrize(std::shared_ptr<Filter> value) override {
        parametrizator(*value);
    }

    static FilterDescriptor from(const std::function<void(Filter)> &parametrizator) {
        return FilterDescriptor(parametrizator);
    }

};


#endif //STREAM_LIB_FILTERDESCRIPTOR_H
