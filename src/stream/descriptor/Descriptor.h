//
// Created by yaroslav on 4/14/24.
//

#ifndef STREAM_LIB_DESCRIPTOR_H
#define STREAM_LIB_DESCRIPTOR_H


#include <memory>
#include "IParametrized.h"

template<class E>
class Descriptor {
    static_assert(std::is_base_of_v<stlb::IParametrized, E>, "E must inherit from IParametrized");

    virtual std::shared_ptr<E> create() = 0;
    virtual void parametrize(std::shared_ptr<E> value) = 0;
};


#endif //STREAM_LIB_DESCRIPTOR_H
