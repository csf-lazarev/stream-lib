//
// Created by yaroslav on 3/9/24.
//

#include "RemapFilter.h"
#include <opencv2/imgproc.hpp>
#include "parameters.h"

void stlb::collection::RemapFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::remap(*img, *processed, map1, map2, interpolation, borderMode, borderValue);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::RemapFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(interpolation, "interpolation")
    BIND_NOCAST_D(borderMode, "border-mode")
}

void stlb::collection::RemapFilter::setObjectP(const std::string &name, void *p) {
    BIND_D(map1, "map1", cv::Mat)
    BIND_D(map2, "map2", cv::Mat)
    BIND_D(borderValue, "border-color", cv::Scalar_<double>)
}
