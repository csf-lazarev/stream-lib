//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_AVERAGINGBLURFILTER_H
#define STREAM_LIB_AVERAGINGBLURFILTER_H


#include <thread>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class AveragingBlurFilter : public DefaultIntermediateFilter {
        int borderType = cv::BORDER_DEFAULT;
        cv::Point anchor = cv::Point(-1, -1);
        cv::Size_<int> kernelSize = cv::Size_<int>(5, 5);
    public:
        ~AveragingBlurFilter() override = default;
        void consume(frame_p frame_ptr) override;
    private:
        void setIntP(const std::string &name, int p) override;
        void setObjectP(const std::string &name, void *p) override;
    };
}


#endif //STREAM_LIB_AVERAGINGBLURFILTER_H
