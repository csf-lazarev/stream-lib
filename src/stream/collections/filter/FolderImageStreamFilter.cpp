//
// Created by yaroslav on 2/20/24.
//

#include "FolderImageStreamFilter.h"
#include <opencv2/opencv.hpp>
#include "Registrar.h"

[[maybe_unused]] const auto filterId = STLB_REG_FILTER(stlb::collection::FolderImageStreamFilter)

namespace fs = std::filesystem;

void stlb::collection::FolderImageStreamFilter::produce(frame_p frame_ptr) {
        this->getOutPipe().writeNewFrame(frame_ptr, NORMAL);
}

frame_p stlb::collection::FolderImageStreamFilter::createFrame() {
    if (iterator == end) {
        throw "End of image folder iterator";
    }
    auto path = *iterator;
    auto src = std::make_shared<cv::Mat>(cv::imread(path));
    auto frame = std::make_shared<Frame>(id++, src);
    ++iterator;
    return frame;
}

void stlb::collection::FolderImageStreamFilter::beforeStart() {
    iterator = paths.begin();
    end = paths.end();
}

bool stlb::collection::FolderImageStreamFilter::canProduce() const {
    return iterator != end;
}

void stlb::collection::FolderImageStreamFilter::setObjectP(const std::string &name, void *p) {
    if (name == "paths") {
        paths = *reinterpret_cast<std::vector<std::string> *>(p);
    }
}