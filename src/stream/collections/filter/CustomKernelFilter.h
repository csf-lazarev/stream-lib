//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_CUSTOMKERNELFILTER_H
#define STREAM_LIB_CUSTOMKERNELFILTER_H


#include "abs/DefaultIntermediateFilter.h"
#include <opencv2/imgproc.hpp>

namespace stlb::collection {
    class CustomKernelFilter : public DefaultIntermediateFilter {
    private:
        int dDepth;
        cv::Mat kernel;
        cv::Point_<int> anchor = cv::Point_(-1, -1);
        double delta = 0;
        int borderType = cv::BORDER_DEFAULT;
    public:

        ~CustomKernelFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setDoubleP(const std::string &name, double p) override;

        void setIntP(const std::string &name, int p) override;

        void setObjectP(const std::string &name, void *p) override;

    };
}


#endif //STREAM_LIB_CUSTOMKERNELFILTER_H
