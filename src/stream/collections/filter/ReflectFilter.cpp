//
// Created by yaroslav on 3/6/24.
//

#include "ReflectFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::ReflectFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto flipped = std::make_shared<cv::Mat>();
    cv::flip(*img, *flipped, flipCode);
    auto out = std::make_shared<Frame>(0, flipped);
    produce(out);
}

void stlb::collection::ReflectFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(flipCode, "flip-code")
}
