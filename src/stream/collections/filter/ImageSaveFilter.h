//
// Created by yaroslav on 2/23/24.
//

#ifndef STREAM_LIB_IMAGESAVEFILTER_H
#define STREAM_LIB_IMAGESAVEFILTER_H


#include <thread>
#include "../../process/filter/AbstractFilter.h"
#include "abs/DefaultTerminateFilter.h"

namespace stlb::collection {
    class ImageSaveFilter : public DefaultTerminateFilter {

    private:
        std::string savePrefix;
        unsigned long saveCounter = 0;
    protected:
        void consume(frame_p frame_ptr) override;

    public:
        void setStringP(const std::string &name, const char *p) override;

    };
}


#endif //STREAM_LIB_IMAGESAVEFILTER_H
