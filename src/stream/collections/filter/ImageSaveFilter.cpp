//
// Created by yaroslav on 2/23/24.
//

#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include "ImageSaveFilter.h"
#include "Registrar.h"

[[maybe_unused]] const auto filterId = STLB_REG_FILTER(stlb::collection::ImageSaveFilter)

void stlb::collection::ImageSaveFilter::consume(frame_p frame_ptr) {
    std::string path = savePrefix + std::to_string(saveCounter++) + ".jpg";
    cv::imwrite(path, *frame_ptr->getImagePtr());
}

void stlb::collection::ImageSaveFilter::setStringP(const std::string &name, const char *p) {
    if (name == "save-prefix") {
        savePrefix = p;
    }
}

