//
// Created by yaroslav on 3/6/24.
//

#include "RotateFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>


void stlb::collection::RotateFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto rotated = std::make_shared<cv::Mat>();
    cv::rotate(*img, *rotated, rotateCode);
    auto out = std::make_shared<Frame>(0, rotated);
    produce(out);
}

void stlb::collection::RotateFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(rotateCode, "rotation")
}

