//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_BILATERALFILTER_H
#define STREAM_LIB_BILATERALFILTER_H


#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class BilateralFilter: public DefaultIntermediateFilter {
    private:
        int diameter = 1;
        double sigmaColor = 0.5;
        double sigmaSpace = 0.5;
        int borderType = cv::BORDER_DEFAULT;

    public:
        ~BilateralFilter() override = default;
        void consume(frame_p frame_ptr) override;
        void setDoubleP(const std::string &name, double p) override;
        void setIntP(const std::string &name, int p) override;
    };
}


#endif //STREAM_LIB_BILATERALFILTER_H
