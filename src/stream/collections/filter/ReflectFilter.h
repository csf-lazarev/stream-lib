//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_REFLECTFILTER_H
#define STREAM_LIB_REFLECTFILTER_H


#include <thread>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class ReflectFilter : public DefaultIntermediateFilter {
        int flipCode = 0;
    public:

        ~ReflectFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setIntP(const std::string &name, int p) override;
    };
}


#endif //STREAM_LIB_REFLECTFILTER_H
