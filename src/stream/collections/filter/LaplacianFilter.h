//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_LAPLACIANFILTER_H
#define STREAM_LIB_LAPLACIANFILTER_H


#include "abs/DefaultIntermediateFilter.h"

#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

namespace stlb::collection {
    class LaplacianFilter : public DefaultIntermediateFilter {
    private:
        int dDepth = CV_16S;
        int kernelSize = 3;
        double scale = 1;
        double delta = 0;
        int borderType = cv::BORDER_DEFAULT;
    public:

        ~LaplacianFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setIntP(const std::string &name, int p) override;

        void setDoubleP(const std::string &name, double p) override;
    };
}


#endif //STREAM_LIB_LAPLACIANFILTER_H
