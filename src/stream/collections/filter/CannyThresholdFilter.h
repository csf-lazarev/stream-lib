//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_CANNYTHRESHOLDFILTER_H
#define STREAM_LIB_CANNYTHRESHOLDFILTER_H


#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class CannyThresholdFilter : public DefaultIntermediateFilter {
    private:
        double lowThreshold = 0;
        double ratio = 3;
        int apertureSize = 3;
        bool l2Gradient;
    public:
        ~CannyThresholdFilter() override = default;
        void consume(frame_p frame_ptr) override;
        void setDoubleP(const std::string &name, double p) override;
        void setIntP(const std::string &name, int p) override;
        void setBoolP(const std::string &name, bool p) override;
    };
}


#endif //STREAM_LIB_CANNYTHRESHOLDFILTER_H
