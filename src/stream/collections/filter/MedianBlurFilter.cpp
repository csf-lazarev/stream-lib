//
// Created by yaroslav on 3/9/24.
//

#include "MedianBlurFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::MedianBlurFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::medianBlur(*img, *processed, kernelSize);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::MedianBlurFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(kernelSize, "kernel-size")
}
