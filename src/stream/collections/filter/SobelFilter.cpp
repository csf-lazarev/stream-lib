//
// Created by yaroslav on 3/9/24.
//

#include "SobelFilter.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::SobelFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::Sobel(*img, *processed, dDepth, dx, dy, kernelSize, scale, delta, borderType);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::SobelFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(scale, "scale")
    BIND_NOCAST_D(delta, "delta")
    BIND_NOCAST_D(borderType, "border-type")
}

void stlb::collection::SobelFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(dDepth, "depth")
    BIND_NOCAST_D(dx, "dx")
    BIND_NOCAST_D(dy, "dy")
    BIND_NOCAST_D(kernelSize, "kernel-size")
}
