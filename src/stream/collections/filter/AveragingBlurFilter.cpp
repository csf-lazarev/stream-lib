//
// Created by yaroslav on 3/6/24.
//

#include "AveragingBlurFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::AveragingBlurFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto blured = std::make_shared<cv::Mat>();
    cv::blur(*img, *blured, kernelSize, anchor, borderType);
    auto out = std::make_shared<Frame>(0, blured);
    produce(out);
}

void stlb::collection::AveragingBlurFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(borderType, "border-type")
}

void stlb::collection::AveragingBlurFilter::setObjectP(const std::string &name, void *p) {
    BIND_D(kernelSize, "kernel-size", cv::Size_<int>)
    BIND_D(anchor, "anchor", cv::Point)
}
