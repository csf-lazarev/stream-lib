//
// Created by yaroslav on 3/6/24.
//

#include "InverseColorFilter.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::InverseColorFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto inverse = std::make_shared<cv::Mat>();
    cv::bitwise_not(*img, *inverse);
    auto out = std::make_shared<Frame>(0, inverse);
    produce(out);
}