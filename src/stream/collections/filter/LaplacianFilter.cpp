//
// Created by yaroslav on 3/9/24.
//

#include "LaplacianFilter.h"
#include "parameters.h"

void stlb::collection::LaplacianFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::Laplacian(*img, *processed, dDepth, kernelSize, scale, delta, borderType);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::LaplacianFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(dDepth, "depth")
    BIND_NOCAST_D(kernelSize, "kernel-size")
    BIND_NOCAST_D(borderType, "border-type")
}

void stlb::collection::LaplacianFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(scale, "scale")
    BIND_NOCAST_D(delta, "delta")
}
