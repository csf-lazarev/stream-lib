//
// Created by yaroslav on 3/9/24.
//

#include "ConvertScaleAbsFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::ConvertScaleAbsFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::convertScaleAbs(*img, *processed, alpha, beta);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::ConvertScaleAbsFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(alpha, "alpha")
    BIND_NOCAST_D(beta, "beta")
}
