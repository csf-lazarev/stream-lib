//
// Created by yaroslav on 3/6/24.
//

#include "ThresholdFilter.h"
#include "../../parameters/parameters.h"

void stlb::collection::ThresholdFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto thres = std::make_shared<cv::Mat>();
    cv::threshold(*img, *thres, threshold, maxThreshold, type);
    auto out = std::make_shared<Frame>(0, thres);
    produce(out);
}

void stlb::collection::ThresholdFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(threshold, "threshold")
    BIND_NOCAST_D(maxThreshold, "max-threshold")
}

void stlb::collection::ThresholdFilter::setObjectP(const std::string &name, void *p) {
    BIND_D(type, "threshold-type", cv::ThresholdTypes)
}
