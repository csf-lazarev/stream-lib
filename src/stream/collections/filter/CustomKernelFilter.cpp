//
// Created by yaroslav on 3/9/24.
//

#include "CustomKernelFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::CustomKernelFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::filter2D(*img, *processed, dDepth, kernel, anchor, delta, borderType);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::CustomKernelFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(delta, "delta")
}

void stlb::collection::CustomKernelFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(dDepth, "depth")
    BIND_NOCAST_D(borderType, "border-type")
}

void stlb::collection::CustomKernelFilter::setObjectP(const std::string &name, void *p) {
    BIND_D(kernel, "kernel", cv::Mat)
    BIND_D(anchor, "anchor", cv::Point_<int>)
}
