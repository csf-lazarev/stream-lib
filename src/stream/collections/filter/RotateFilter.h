//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_ROTATEFILTER_H
#define STREAM_LIB_ROTATEFILTER_H


#include <thread>
#include <semaphore>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class RotateFilter : public DefaultIntermediateFilter {
    private:
        int rotateCode = cv::ROTATE_180;
    public:

        ~RotateFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setIntP(const std::string &name, int p) override;
    };
}


#endif //STREAM_LIB_ROTATEFILTER_H
