//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_MEDIANBLURFILTER_H
#define STREAM_LIB_MEDIANBLURFILTER_H


#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class MedianBlurFilter : public DefaultIntermediateFilter {
    private:
        int kernelSize = 3;
    public:

        ~MedianBlurFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setIntP(const std::string &name, int p) override;

    };
}


#endif //STREAM_LIB_MEDIANBLURFILTER_H
