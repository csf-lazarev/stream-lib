//
// Created by yaroslav on 2/20/24.
//

#include "ImageResizeFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>
#include "Registrar.h"

[[maybe_unused]] const auto filterId = STLB_REG_FILTER(stlb::collection::ImageResizeFilter)

void stlb::collection::ImageResizeFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto resized = std::make_shared<cv::Mat>();
    cv::resize(*img, *resized, cv::Size(static_cast<int>(img->cols * factorX), static_cast<int>(img->rows * factorY)));
    auto out = std::make_shared<Frame>(0, resized);
    produce(out);
}

void stlb::collection::ImageResizeFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(factorX, "factor-x")
    BIND_NOCAST_D(factorY, "factor-y")
    if (name == "factor") {
        factorX = p;
        factorY = p;
        return;
    }
}

