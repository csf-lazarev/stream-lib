//
// Created by yaroslav on 3/6/24.
//

#include "GrayScaleFilter.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::GrayScaleFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto gray = std::make_shared<cv::Mat>();
    cv::cvtColor(*img, *gray, cv::COLOR_RGB2GRAY);
    auto out = std::make_shared<Frame>(0, gray);
    produce(out);
}