//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_INVERSECOLORFILTER_H
#define STREAM_LIB_INVERSECOLORFILTER_H


#include <thread>
#include <semaphore>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class InverseColorFilter : public DefaultIntermediateFilter {
    public:
        ~InverseColorFilter() override = default;

        void consume(frame_p frame_ptr) override;
    };
}


#endif //STREAM_LIB_INVERSECOLORFILTER_H
