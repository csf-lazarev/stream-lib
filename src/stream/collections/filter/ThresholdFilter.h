//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_THRESHOLDFILTER_H
#define STREAM_LIB_THRESHOLDFILTER_H


#include <thread>
#include <semaphore>
#include <opencv2/imgproc.hpp>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class ThresholdFilter : public DefaultIntermediateFilter {
    private:
        double threshold;
        double maxThreshold;
        cv::ThresholdTypes type;
    public:

        ~ThresholdFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setDoubleP(const std::string &name, double p) override;

        void setObjectP(const std::string &name, void *p) override;

    };
}


#endif //STREAM_LIB_THRESHOLDFILTER_H
