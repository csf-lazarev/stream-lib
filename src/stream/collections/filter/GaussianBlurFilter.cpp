//
// Created by yaroslav on 3/6/24.
//

#include "GaussianBlurFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::GaussianBlurFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto gray = std::make_shared<cv::Mat>();
    cv::GaussianBlur(*img, *gray, ksize, sigmaX, sigmaY, borderType);
    auto out = std::make_shared<Frame>(0, gray);
    produce(out);
}

void stlb::collection::GaussianBlurFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(sigmaX, "sigma-x")
    BIND_NOCAST_D(sigmaY, "sigma-y")
}

void stlb::collection::GaussianBlurFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(borderType, "border-type")
}

void stlb::collection::GaussianBlurFilter::setObjectP(const std::string &name, void *p) {
    BIND_D(ksize, "kernel-size", cv::Size_<int>)
}
