//
// Created by yaroslav on 3/9/24.
//

#include "AddBorderFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::AddBorderFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto gray = std::make_shared<cv::Mat>();
    auto top = (int) (borderFactor * img->rows);
    int bottom = top;
    auto left = (int) (borderFactor * img->cols);
    int right = left;
    cv::copyMakeBorder(*img, *gray, top, bottom, left, right, borderType, color);
    auto out = std::make_shared<Frame>(0, gray);
    produce(out);
}

void stlb::collection::AddBorderFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(borderFactor, "border-factor")
}

void stlb::collection::AddBorderFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(borderType, "border-type")
}

void stlb::collection::AddBorderFilter::setObjectP(const std::string &name, void *p) {
    BIND_D(color, "border-color-rgb", cv::Scalar)
}
