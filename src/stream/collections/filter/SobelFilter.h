//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_SOBELFILTER_H
#define STREAM_LIB_SOBELFILTER_H


#include "abs/DefaultIntermediateFilter.h"
#include "parameters.h"

namespace stlb::collection {
    class SobelFilter : public DefaultIntermediateFilter {
    private:
        int dDepth = CV_16S;
        int dx = 1;
        int dy = 0;
        int kernelSize = 3;
        double scale = 1;
        double delta = 1;
        int borderType = cv::BORDER_DEFAULT;
    public:

        ~SobelFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setDoubleP(const std::string &name, double p) override;

        void setIntP(const std::string &name, int p) override;

    };
}


#endif //STREAM_LIB_SOBELFILTER_H
