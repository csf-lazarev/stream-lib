//
// Created by yaroslav on 3/9/24.
//

#include "BilateralFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::BilateralFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::bilateralFilter(*img, *processed, diameter, sigmaColor, sigmaSpace, borderType);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::BilateralFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(sigmaColor, "sigma-color")
    BIND_NOCAST_D(sigmaSpace, "sigma-space")
}

void stlb::collection::BilateralFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(diameter, "diameter")
    BIND_NOCAST_D(borderType , "border-type")
}
