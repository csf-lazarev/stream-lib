//
// Created by yaroslav on 2/20/24.
//

#ifndef STREAM_LIB_FOLDERIMAGESTREAMFILTER_H
#define STREAM_LIB_FOLDERIMAGESTREAMFILTER_H


#include <semaphore>
#include <thread>
#include "../../data/buffer/CircularBuffer.h"
#include "abs/DefaultProduceFilter.h"

namespace stlb::collection {
    class FolderImageStreamFilter : public DefaultProduceFilter {
    private:
        frame_id_t id = 1;
        std::vector<std::string> paths = std::vector<std::string>();
        std::vector<std::string>::iterator iterator;
        std::vector<std::string>::iterator end;
    public:
        ~FolderImageStreamFilter() override = default;

        void produce(frame_p frame_ptr) override;

        frame_p createFrame() override;

        void beforeStart() override;

        bool canProduce() const override;

        void setObjectP(const std::string &name, void *p) override;
    };
}

#endif //STREAM_LIB_FOLDERIMAGESTREAMFILTER_H
