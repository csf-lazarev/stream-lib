//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_ADDBORDERFILTER_H
#define STREAM_LIB_ADDBORDERFILTER_H


#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class AddBorderFilter : public DefaultIntermediateFilter {
    private:
        double borderFactor = 0.05;
        int borderType;
        cv::Scalar color;

    public:

        ~AddBorderFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setDoubleP(const std::string &name, double p) override;

        void setIntP(const std::string &name, int p) override;

        void setObjectP(const std::string &name, void *p) override;
    };
}


#endif //STREAM_LIB_ADDBORDERFILTER_H
