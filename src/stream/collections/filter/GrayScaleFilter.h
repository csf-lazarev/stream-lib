//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_GRAYSCALEFILTER_H
#define STREAM_LIB_GRAYSCALEFILTER_H

#include <thread>
#include <semaphore>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class GrayScaleFilter : public DefaultIntermediateFilter {
    public:
        ~GrayScaleFilter() override = default;

        void consume(frame_p frame_ptr) override;
    };
}


#endif //STREAM_LIB_GRAYSCALEFILTER_H
