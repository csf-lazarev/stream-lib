//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_COLORTRANSFORMATIONFILTER_H
#define STREAM_LIB_COLORTRANSFORMATIONFILTER_H


#include "abs/DefaultIntermediateFilter.h"
#include <opencv2/imgproc.hpp>

namespace stlb::collection {
    class ColorTransformationFilter : public DefaultIntermediateFilter {
    private:
        int color =  cv::COLOR_BGR2GRAY;
    public:
        ~ColorTransformationFilter() override = default;
        void consume(frame_p frame_ptr) override;
        void setIntP(const std::string &name, int p) override;
    };
}


#endif //STREAM_LIB_COLORTRANSFORMATIONFILTER_H
