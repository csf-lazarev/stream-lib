//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_CONVERTSCALEABSFILTER_H
#define STREAM_LIB_CONVERTSCALEABSFILTER_H


#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class ConvertScaleAbsFilter : public DefaultIntermediateFilter {
    private:
        double alpha = 1;
        double beta = 0;
    public:
        ~ConvertScaleAbsFilter() override = default;
        void consume(frame_p frame_ptr) override;
        void setDoubleP(const std::string &name, double p) override;
    };
}


#endif //STREAM_LIB_CONVERTSCALEABSFILTER_H
