//
// Created by yaroslav on 3/6/24.
//

#ifndef STREAM_LIB_DEFAULTINTERMEDIATEFILTER_H
#define STREAM_LIB_DEFAULTINTERMEDIATEFILTER_H


#include "../../../process/filter/AbstractFilter.h"

namespace stlb {
    class DefaultIntermediateFilter : public AbstractFilter {
    private:
        std::shared_ptr<IPipe> inputPipe;
        std::shared_ptr<IPipe> outPipe;

    public:
        [[nodiscard]] IPipe & getInPipe() override;

        [[nodiscard]] IPipe & getOutPipe() override;

        ~DefaultIntermediateFilter() override = default;

        void connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) override;

    protected:
        void produce(frame_p frame_ptr) override;
    };
}


#endif //STREAM_LIB_DEFAULTINTERMEDIATEFILTER_H
