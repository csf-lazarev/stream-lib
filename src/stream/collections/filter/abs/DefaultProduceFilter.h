//
// Created by yaroslav on 4/6/24.
//

#ifndef STREAM_LIB_DEFAULTPRODUCEFILTER_H
#define STREAM_LIB_DEFAULTPRODUCEFILTER_H


#include "../../../process/filter/AbstractFilter.h"

namespace stlb {
    class DefaultProduceFilter : public AbstractFilter {
        std::shared_ptr<IPipe> outPipe;
    public:
        [[nodiscard]] IPipe & getOutPipe() override;

        ~DefaultProduceFilter() override = default;

        void connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) override;

        virtual frame_p createFrame() = 0;

        [[nodiscard]] virtual bool canProduce() const = 0;


    };
}

#endif //STREAM_LIB_DEFAULTPRODUCEFILTER_H
