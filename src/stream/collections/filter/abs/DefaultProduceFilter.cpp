//
// Created by yaroslav on 4/6/24.
//

#include "DefaultProduceFilter.h"

stlb::IPipe &stlb::DefaultProduceFilter::getOutPipe() {
    return *outPipe;
}

void stlb::DefaultProduceFilter::connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) {
    if (flag == PIPE_OUT) {
        outPipe = pipe;
    }
}