//
// Created by yaroslav on 4/6/24.
//

#ifndef STREAM_LIB_DEFAULTTERMINATEFILTER_H
#define STREAM_LIB_DEFAULTTERMINATEFILTER_H


#include "../../../process/filter/AbstractFilter.h"

namespace stlb {
    class DefaultTerminateFilter : public AbstractFilter {
        std::shared_ptr<IPipe> inputPipe;
    public:
        [[nodiscard]] IPipe & getInPipe() override;

        ~DefaultTerminateFilter() override = default;

        void connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) override;
    };
}

#endif //STREAM_LIB_DEFAULTTERMINATEFILTER_H
