//
// Created by yaroslav on 3/6/24.
//

#include "DefaultIntermediateFilter.h"

void stlb::DefaultIntermediateFilter::produce(frame_p frame_ptr) {
    outPipe->writeNewFrame(frame_ptr, NORMAL);
}

stlb::IPipe & stlb::DefaultIntermediateFilter::getInPipe() {
    return inputPipe ?  *inputPipe : IPipe::empty();
}

stlb::IPipe & stlb::DefaultIntermediateFilter::getOutPipe() {
    return outPipe ? *outPipe : IPipe::empty();
}

void stlb::DefaultIntermediateFilter::connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) {
    if (flag == PIPE_IN) {
        inputPipe = pipe;
    }
    if (flag == PIPE_OUT) {
        outPipe = pipe;
    }
}
