//
// Created by yaroslav on 4/6/24.
//

#include "DefaultTerminateFilter.h"

stlb::IPipe &stlb::DefaultTerminateFilter::getInPipe() {
    return *inputPipe;
}

void stlb::DefaultTerminateFilter::connectPipe(std::shared_ptr<IPipe> pipe, pipe_flag_t flag) {
    if (flag == PIPE_IN) {
        inputPipe = pipe;
    }
}
