//
// Created by yaroslav on 3/9/24.
//

#include "ColorTransformationFilter.h"
#include "parameters.h"

void stlb::collection::ColorTransformationFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::cvtColor(*img, *processed, color);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::ColorTransformationFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(color, "color")
}
