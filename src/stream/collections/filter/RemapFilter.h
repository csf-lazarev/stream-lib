//
// Created by yaroslav on 3/9/24.
//

#ifndef STREAM_LIB_REMAPFILTER_H
#define STREAM_LIB_REMAPFILTER_H


#include "abs/DefaultIntermediateFilter.h"
#include <opencv2/imgproc.hpp>

namespace stlb::collection {
    class RemapFilter : public DefaultIntermediateFilter {
    private:
        cv::Mat map1;
        cv::Mat map2;
        int interpolation;
        int borderMode;
        cv::Scalar_<double> borderValue = cv::Scalar_<double>();
    public:

        ~RemapFilter() override = default;

        void consume(frame_p frame_ptr) override;

        void setIntP(const std::string &name, int p) override;

        void setObjectP(const std::string &name, void *p) override;
    };
}


#endif //STREAM_LIB_REMAPFILTER_H
