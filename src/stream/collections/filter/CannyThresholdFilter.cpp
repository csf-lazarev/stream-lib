//
// Created by yaroslav on 3/9/24.
//

#include "CannyThresholdFilter.h"
#include "parameters.h"
#include <opencv2/imgproc.hpp>

void stlb::collection::CannyThresholdFilter::consume(frame_p frame_ptr) {
    if (frame_ptr == nullptr) { return; }
    auto img = frame_ptr->getImagePtr();
    auto processed = std::make_shared<cv::Mat>();
    cv::Canny(*img, *processed, lowThreshold, lowThreshold * ratio, apertureSize, l2Gradient);
    auto out = std::make_shared<Frame>(0, processed);
    produce(out);
}

void stlb::collection::CannyThresholdFilter::setDoubleP(const std::string &name, double p) {
    BIND_NOCAST_D(lowThreshold, "low-threshold")
    BIND_NOCAST_D(ratio, "ratio")
}

void stlb::collection::CannyThresholdFilter::setIntP(const std::string &name, int p) {
    BIND_NOCAST_D(apertureSize, "aperture-size")
}

void stlb::collection::CannyThresholdFilter::setBoolP(const std::string &name, bool p) {
    BIND_NOCAST_D(l2Gradient, "l2gradient")
}
