//
// Created by yaroslav on 2/20/24.
//

#ifndef STREAM_LIB_IMAGERESIZEFILTER_H
#define STREAM_LIB_IMAGERESIZEFILTER_H

#include <thread>
#include "abs/DefaultIntermediateFilter.h"

namespace stlb::collection {
    class ImageResizeFilter : public DefaultIntermediateFilter {
    private:
        double factorX = 2;
        double factorY = 2;
    public:
        ~ImageResizeFilter() override = default;
        void consume(frame_p frame_ptr) override;
        void setDoubleP(const std::string &name, double p) override;

    };
}


#endif //STREAM_LIB_IMAGERESIZEFILTER_H
