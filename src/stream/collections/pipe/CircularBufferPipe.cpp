//
// Created by yaroslav on 2/21/24.
//

#include "CircularBufferPipe.h"

frame_p stlb::CircularBufferPipe::readCurrentFrame() {
    if (currentWriteFrame - currentReadFrame > 0) {
        auto frame = frames[currentReadFrame % bufferSize];
        currentReadFrame++;
        return frame;
    }
    return nullptr;
}

int stlb::CircularBufferPipe::writeNewFrame(std::shared_ptr<Frame> frame, write_flag_t flag) {
    if (currentWriteFrame - currentReadFrame >= bufferSize) {
        return -1;
    }
    frames[currentWriteFrame % bufferSize] = frame;
    currentWriteFrame++;
    consumer->trigger();
    return 0;
}

void stlb::CircularBufferPipe::attachTriggerable(ITriggerable *triggerable, trig_flag_t flag) {
    if (flag == T_TO) {
        consumer = triggerable;
    }
}

stlb::CircularBufferPipe::CircularBufferPipe(size_t bufferSize) : bufferSize(bufferSize),
                                                                  frames(bufferSize) {
}
