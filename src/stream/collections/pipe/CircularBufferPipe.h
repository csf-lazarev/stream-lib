//
// Created by yaroslav on 2/21/24.
//

#ifndef STREAM_LIB_CIRCULARBUFFERPIPE_H
#define STREAM_LIB_CIRCULARBUFFERPIPE_H


#include "../../data/pipe/IPipe.h"

namespace stlb {
    class CircularBufferPipe : public IPipe {

    private:
        size_t bufferSize = 20;
        unsigned long currentReadFrame = 0;
        unsigned long currentWriteFrame = 0;
        std::vector<frame_p> frames;

        ITriggerable *consumer;

    public:
        frame_p readCurrentFrame() override;

        int writeNewFrame(std::shared_ptr<Frame> frame, write_flag_t flag) override;

        void attachTriggerable(ITriggerable *triggerable, trig_flag_t flag) override;

        CircularBufferPipe(size_t bufferSize);
    };
}


#endif //STREAM_LIB_CIRCULARBUFFERPIPE_H
